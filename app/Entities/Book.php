<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = "books";

    protected $fillable = [
        'name',
        'pages',
        'ISBN',
        'price',
        'published_at',
        'published_by',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'published_by', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'books_categories',
            'book_id',
            'category_id'
        );
    }

    public function authors()
    {
        return $this->belongsToMany(
            Author::class,
            'books_authors',
            'book_id',
            'author_id'
        );
    }


}

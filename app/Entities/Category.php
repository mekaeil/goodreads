<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    protected $fillable = [
        'name',
        'slug',
    ];

    public function books()
    {
        return $this->belongsToMany(
            Book::class,
            'books_categories',
            'category_id',
            'book_id'
        );
    }

}

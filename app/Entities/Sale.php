<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = "sales";

    protected $fillable = [
        'region',
        'country',
        'type',
        'sales_channel', // Offline, Online
        'order_date',
        'order_id',
        'ship_date',
        'unit_sold',
        'unit_price',
        'unit_cost',
        'total_revenue',
        'total_cost',
        'total_profit',
    ];

}

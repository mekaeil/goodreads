<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Author;

class AuthorsController extends Controller
{
    public function index()
    {
        $authors = Author::paginate(15);

        return view('panel.author.index', compact('authors'));
    }

    public function create()
    {
        return view('panel.author.create');
    }

    public function edit(Author $author)
    {
        return view('panel.author.edit', compact('author'));
    }

    //// need validation :)
    public function store(Request $request)
    {
        Author::create([
            'name'      => $request->name,
            'birthday'  => $request->birthday,
        ]);

        return redirect()->route('panel.author.index')->with('message',[
            'type'  => 'success',
            'text'  => 'The Author created successfully!'
        ]);
    }

    //// need validation :)
    public function update(Author $author,Request $request)
    {
        $author->update([
            'name'      => $request->name,
            'birthday'  => $request->birthday,
        ]);

        return redirect()->route('panel.author.index')->with('message',[
            'type'  => 'success',
            'text'  => 'The Author updated!'
        ]);
    }

    public function delete(Author $author)
    {
        $author->delete();

        return redirect()->route('panel.author.index')->with('message',[
            'type'  => 'warning',
            'text'  => 'The Author deleted!'
        ]);

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Book;
use App\Entities\Author;
use App\Entities\Category;
use App\Events\BookCreated;

class BooksController extends Controller
{

    public function index()
    {
        $books = Book::paginate(15);

        return view('panel.book.index', compact('books'));
    }

    public function create()
    {
        $authors    = Author::get()->pluck('name','id');
        $categories = Category::get()->pluck('name','id');

        return view('panel.book.create', compact('authors', 'categories'));
    }

    public function edit(Book $book)
    {
        $authors    = Author::get()->pluck('name','id');
        $selected   = $book->authors->pluck('id','id')->toArray();

        $categories = Category::get()->pluck('name','id');
        $catSelected= $book->categories->pluck('id','id')->toArray();

        return view('panel.book.edit', compact('book', 'authors', 'selected', 'categories', 'catSelected'));
    }

    //// need validation :)
    public function update(Book $book, Request $request)
    {

        $book->update([
            'name'          => $request->name,
            'pages'         => $request->pages,
            'ISBN'          => $request->ISBN,
            'price'         => $request->price,
            'published_at'  => $request->published_at,
        ]);

        ///// ATTACH AUTHORS
        /////////////////////////////////////////////////////////////////
        ($book->authors) ?  $book->authors()->detach() : '';
        if(! empty($request->authors))
        {
            $book->authors()->attach($request->authors);
        }

        ///// ATTACH CATEGORY
        /////////////////////////////////////////////////////////////////
        ($book->categories) ?  $book->categories()->detach() : '';
        if(! empty($request->categories))
        {
            $book->categories()->attach($request->categories);
        }

        return redirect()->route('panel.book.index')->with('message',[
            'type'  => 'success',
            'text'  => 'The book updated successfully!'
        ]);
    }

    //// need validation :)
    public function store(Request $request)
    {
        $user = \Auth::user();

        $book = Book::create([
            'name'          => $request->name,
            'pages'         => $request->pages,
            'ISBN'          => $request->ISBN,
            'price'         => $request->price,
            'published_at'  => $request->published_at,
            'published_by'  => $user->id,
        ]);

        if(! empty($request->authors))
        {
            $book->authors()->attach($request->authors);
        }

        if(! empty($request->categories))
        {
            $book->categories()->attach($request->categories);
        }

        event(new BookCreated($user, $book));

        return redirect()->route('panel.book.index')->with('message',[
            'type'  => 'success',
            'text'  => 'The book created successfully!'
        ]);
    }

    public function delete(Book $book)
    {
        $book->delete();

        return redirect()->route('panel.book.index')->with('message',[
            'type'  => 'warning',
            'text'  => 'The book deleted successfully!'
        ]);
    }

}

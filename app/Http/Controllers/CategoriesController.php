<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::paginate(15);

        return view('panel.category.index', compact('categories'));
    }

    public function create()
    {
        return view('panel.category.create');
    }

    public function edit(Category $category)
    {
        return view('panel.category.edit', compact('category'));
    }

    //// need validation
    public function store(Request $request)
    {
        Category::create([
            'name'  => $request->name,
            'slug'  => $request->slug,
        ]);

        return redirect()->route('panel.category.index')->with('message',[
            'type'  => 'success',
            'text'  => 'This Category created successfully!',
        ]);
    }

    //// need validation
    public function update(Category $category, Request $request)
    {
        $category->update([
            'name'  => $request->name,
            'slug'  => $request->slug,
        ]);

        return redirect()->route('panel.category.index')->with('message',[
            'type'  => 'success',
            'text'  => 'This Category updated successfully!',
        ]);
    }

    public function delete(Category $category)
    {
        $category->delete();

        return redirect()->route('panel.category.index')->with('message',[
            'type'  => 'warning',
            'text'  => 'Category deleted successfully!',
        ]);
    }

}

<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use App\Entities\Sale;
use Illuminate\Routing\Controller;
use App\Imports\SalesImport;

class SalesController extends Controller
{

    public function index()
    {
        $sales = Sale::paginate(15);
        return view('panel.sales.index', compact('sales'));
    }

    public function importForm()
    {
        return view('panel.sales.import');
    }

    public function importData(Request $request)
    {

        Excel::import(new SalesImport, request()->file('file'));

        return redirect()->route('panel.sales.index')->with('message',[
            'type'  => 'success',
            'text'  => 'File imported successfully!',
        ]);
    }


}

<?php

namespace App\Imports;

use App\Entities\Sale;
use Maatwebsite\Excel\Concerns\ToModel;

class SalesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Sale([
            'region'        => $row[0],
            'country'       => $row[1],
            'type'          => $row[2],
            'sales_channel' => $row[3],
            'order_date'    => $row[4],
            'order_id'      => $row[5],
            'ship_date'     => $row[6],
            'unit_sold'     => $row[7],
            'unit_price'    => $row[8],
            'unit_cost'     => $row[9],
            'total_revenue' => $row[10],
            'total_cost'    => $row[11],
            'total_profit'  => $row[12],
        ]);
    }
}

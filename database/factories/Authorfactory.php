<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\Entities\Author;

$factory->define(Author::class, function (Faker $faker) {
    return [
        'name'      => $faker->name,
        'birthday'  => $faker->dateTimeThisCentury->format('Y-m-d'),
    ];
});

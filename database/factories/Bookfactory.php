<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\Entities\Book;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name'          => $faker->name,
        'pages'         => $faker->randomNumber(5),
        'ISBN'          => $faker->name,
        'price'         => $faker->randomNumber(5),
        'published_at'  => now(),
        // 'published_by'  => 1,
    ];
});

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\Entities\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name'  => $faker->word,
        'slug'  => $faker->slug,
    ];
});

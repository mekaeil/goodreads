<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Book;
use App\Entities\Author;
use App\Entities\Category;
use App\Entities\User;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(User::class, 5)->create()->each(function($user){

            factory(Book::class, 5)->create(['published_by' => $user->id])->each(function($book){
                $book->categories()->attach(factory(Category::class, rand(1,3))->create());
                $book->authors()->attach(factory(Author::class, rand(1,3))->create());
            });

            $this->command->info('User with book, category and author created');

        });
    }
}

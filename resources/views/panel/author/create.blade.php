@extends('panel.master')

@section('header')
    @parent

@endsection

@section('breadcrumb')
    @include('panel.layouts.breadcrumb',[
        'pageTitle' => 'Create New Author',
        'lists' => [
            [
                'link'  => 'home',
                'name'  => 'Dashboard',
            ],
            [
                'link'  => 'panel.author.index',
                'name'  => 'Authors',
            ],
            [
                'link'  => '#',
                'name'  => 'New Author',
            ]
        ]
    ])
@endsection

@section('content')

<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">

            <form class="forms-sample" method="POST" action="{{ route('panel.author.store') }}">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name like: Mekaeil Andisheh">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="birthday">Birthday</label>
                            <input type="date" name="birthday" class="form-control" id="birthday" placeholder="Birthday">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <a href="{{ route('panel.author.index') }}" class="btn btn-light">Cancel</a>
            </form>
        </div>
    </div>
</div>

@endsection


@section('footer')
    @parent

@endsection

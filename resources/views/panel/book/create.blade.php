@extends('panel.master')

@section('header')
    @parent

@endsection

@section('breadcrumb')
    @include('panel.layouts.breadcrumb',[
        'pageTitle' => 'Create New Book',
        'lists' => [
            [
                'link'  => 'home',
                'name'  => 'Dashboard',
            ],
            [
                'link'  => 'panel.book.index',
                'name'  => 'Books',
            ],
            [
                'link'  => '#',
                'name'  => 'New Book',
            ]
        ]
    ])
@endsection

@section('content')

<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">

            <form class="forms-sample" method="POST" action="{{ route('panel.book.store') }}">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name like: You don't know Javascript">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="pages">Pages</label>
                            <input type="number" name="pages" class="form-control" id="pages" placeholder="Number of pages">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="categories">Categories</label>
                            <select multiple class="form-control" name="categories[]" id="categories">
                                @foreach ($categories as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="authors">Authors</label>
                            <select multiple class="form-control" name="authors[]" id="authors">
                                @foreach ($authors as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="ISBN">ISBN</label>
                            <input type="text" class="form-control" name="ISBN" id="ISBN" placeholder="ISBN ...">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="number" name="price" class="form-control" id="price" value="price book ">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="published_at">Published at</label>
                            <input type="date" name="published_at" class="form-control" id="published_at" placeholder="Published at">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <a href="{{ route('panel.book.index') }}" class="btn btn-light">Cancel</a>
            </form>
        </div>
    </div>
</div>

@endsection


@section('footer')
    @parent

@endsection

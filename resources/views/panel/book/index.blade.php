@extends('panel.master')

@section('header')
    @parent

@endsection

@section('breadcrumb')
    @include('panel.layouts.breadcrumb',[
        'pageTitle' => 'Books',
        'lists' => [
            [
                'link'  => 'home',
                'name'  => 'Dashboard',
            ],
            [
                'link'  => '#',
                'name'  => 'Books',
            ]
        ]
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('panel.book.create') }}" class="btn btn-outline-primary btn-icon-text float-right btn-newInList">
                        <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                        New book
                    </a>
                    <h4 class="card-title">List of the books</h4>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Book Name
                                </th>
                                <th>
                                    ISBN
                                </th>
                                <th>
                                    Pages
                                </th>
                                <th>
                                    Published at
                                </th>
                                <th>
                                    Authors
                                </th>
                                <th>
                                    Categories
                                </th>
                                <th>
                                    Published By
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($books as $item)
                                <tr>
                                    <td>
                                        {{ $item->id }}
                                    </td>
                                    <td>
                                        {{ $item->name }}
                                    </td>
                                    <td>
                                        {{ $item->ISBN ??'--'  }}
                                    </td>
                                    <td>
                                        {{ $item->pages }}
                                    </td>
                                    <td>
                                        {{ $item->published_at }}
                                    </td>
                                    <td>
                                        @forelse ($item->authors as $author)
                                            <span>{{ $author->name }}</span>,<br>
                                        @empty
                                            ----
                                        @endforelse
                                    </td>
                                    <td>
                                        @forelse ($item->categories as $cat)
                                            <span>{{ $cat->name }}</span>,<br>
                                        @empty
                                            ----
                                        @endforelse
                                    </td>
                                    <td>
                                        {{ $item->user->first_name . " " . $item->user->last_name }}
                                    </td>
                                    <td>
                                        @if( $item->published_by == \Auth::user()->id )
                                            <a href="{{ route('panel.book.edit', $item) }}" class="btn btn-outline-dark btn-sm">Edit</a>

                                            <form action="{{ route('panel.book.delete', $item) }}" method="post" class="inline-block">
                                                @method('DELETE')
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                            </form>
                                        @else
                                            ----
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <div class="pagination">
                        {{ $books->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer')
    @parent

@endsection

@extends('panel.master')

@section('header')
    @parent

@endsection

@section('breadcrumb')
    @include('panel.layouts.breadcrumb',[
        'pageTitle' => 'Categories',
        'lists' => [
            [
                'link'  => 'home',
                'name'  => 'Dashboard',
            ],
            [
                'link'  => '#',
                'name'  => 'Categories',
            ]
        ]
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('panel.category.create') }}" class="btn btn-outline-primary btn-icon-text float-right btn-newInList">
                        <i class="mdi mdi-account-multiple-plus btn-icon-prepend"></i>
                        New category
                    </a>
                    <h4 class="card-title">List of the category</h4>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Category Name
                                </th>
                                <th>
                                    Slug
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $item)
                                <tr>
                                    <td>
                                        {{ $item->id }}
                                    </td>
                                    <td>
                                        {{ $item->name }}
                                    </td>
                                    <td>
                                        {{ $item->slug }}
                                    </td>
                                    <td>
                                        <a href="{{ route('panel.category.edit', $item) }}" class="btn btn-outline-dark btn-sm">Edit</a>

                                        <form action="{{ route('panel.category.delete', $item) }}" method="post" class="inline-block">
                                            @method('DELETE')
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <div class="pagination">
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer')
    @parent

@endsection

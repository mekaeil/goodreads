    <!-- plugins:js -->
    <script src="{{ asset('panel/vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('panel/vendors/js/vendor.bundle.addons.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('panel/js/off-canvas.js') }}"></script>
    <script src="{{ asset('panel/js/misc.js') }}"></script>
    <!-- endinject -->

    @yield('footer')
</body>

</html>

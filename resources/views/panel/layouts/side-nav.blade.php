
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">

      <li class="nav-item">
        <a class="nav-link " href="{{ route('home') }}">
          Dashboard
          <i class="mdi mdi-home menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('panel.book.index') }}">
          Books
          <i class="mdi mdi-book-open-variant menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('panel.category.index') }}">
            Categories
            <i class="mdi mdi-checkbox-multiple-blank-outline menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('panel.author.index') }}">
          Authors
          <i class="mdi mdi-account-convert menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('panel.sales.index') }}">
          Sales
          <i class="mdi mdi-account-convert menu-icon"></i>
        </a>
      </li>
      {{-- <li class="nav-item">
        <a class="nav-link" href="#">
          Users
          <i class="mdi mdi-account-multiple menu-icon"></i>
        </a>
      </li> --}}
  </ul>
</nav>

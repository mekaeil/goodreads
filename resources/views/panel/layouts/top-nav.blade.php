<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{{ url('/') }}">
            Goodreads
        </a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">

            <li class="nav-item d-none d-lg-block full-screen-link">
                <a class="nav-link">
                <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
                </a>
            </li>
            <li class="nav-item nav-logout d-none d-lg-block">
                <form id="logout-form" class="nav-link" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" style="background: none;border: none;color: #9c9fa6;"><i class="mdi mdi-power"></i></button>
                </form>

            </li>
        </ul>
    </div>
</nav>

@extends('panel.master')

@section('header')
    @parent

@endsection

@section('breadcrumb')
    @include('panel.layouts.breadcrumb',[
        'pageTitle' => 'Import file',
        'lists' => [
            [
                'link'  => 'home',
                'name'  => 'Dashboard',
            ],
            [
                'link'  => 'panel.sales.index',
                'name'  => 'Sales',
            ],
            [
                'link'  => '#',
                'name'  => 'Import file',
            ]
        ]
    ])
@endsection

@section('content')

<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">

            <form class="forms-sample" method="POST" action="{{ route('panel.sales.upload.data') }}" enctype="multipart/form-data">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="file">Import data</label>
                            <input type="file" name="file" class="form-control" id="file">
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <a href="{{ route('panel.sales.index') }}" class="btn btn-light">Cancel</a>
            </form>
        </div>
    </div>
</div>

@endsection


@section('footer')
    @parent

@endsection

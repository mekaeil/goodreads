@extends('panel.master')

@section('header')
    @parent

@endsection

@section('breadcrumb')
    @include('panel.layouts.breadcrumb',[
        'pageTitle' => 'Books',
        'lists' => [
            [
                'link'  => 'home',
                'name'  => 'Dashboard',
            ],
            [
                'link'  => '#',
                'name'  => 'Sales',
            ]
        ]
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('panel.sales.import.form') }}" class="btn btn-outline-primary btn-icon-text float-right btn-newInList">
                        <i class="mdi mdi-account-multiple-plus btn-icon-prepend"></i>
                        Import Data
                    </a>
                    <h4 class="card-title">List of the sales</h4>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    Order ID
                                </th>
                                <th>
                                    Region
                                </th>
                                <th>
                                    Country
                                </th>
                                <th>
                                    Item Type
                                </th>
                                <th>
                                    Sales Channel
                                </th>
                                <th>
                                    Order date
                                </th>
                                <th>
                                    Ship Date
                                </th>
                                <th>
                                    Unit Sold
                                </th>
                                <th>
                                    Unit Price
                                </th>
                                <th>
                                    Unit Cost
                                </th>
                                <th>
                                    Total Revenue
                                </th>
                                <th>
                                    Total Cost
                                </th>
                                <th>
                                    Total Profit
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sales as $item)
                                <tr>
                                    <td>
                                        {{ $item->order_id }}
                                    </td>
                                    <td>
                                        {{ $item->region }}
                                    </td>
                                    <td>
                                        {{ $item->country }}
                                    </td>
                                    <td>
                                        {{ $item->type }}
                                    </td>
                                    <td>
                                        {{ $item->sales_channel }}
                                    </td>
                                    <td>
                                        {{ $item->order_date }}
                                    </td>
                                    <td>
                                        {{ $item->ship_date }}
                                    </td>
                                    <td>
                                        {{ $item->unit_sold }}
                                    </td>
                                    <td>
                                        {{ $item->unit_price }}
                                    </td>
                                    <td>
                                        {{ $item->unit_cost }}
                                    </td>
                                    <td>
                                        {{ $item->total_revenue }}
                                    </td>
                                    <td>
                                        {{ $item->total_cost }}
                                    </td>
                                    <td>
                                        {{ $item->total_profit }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="pagination">
                        {{ $sales->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer')
    @parent

@endsection

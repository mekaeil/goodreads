<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::group([
        'prefix'    => 'dashboard',
        'as'        => 'panel.',
        'middleware'=> ['web', 'auth'],
    ],
    function () {

        Route::group([
            'prefix'    => 'book',
            'as'        => 'book.',
        ],
        function()
        {
            // panel.book.index
            Route::get('/', 'BooksController@index')->name('index');

            // panel.book.create
            Route::get('create', 'BooksController@create')->name('create');

            // panel.book.store
            Route::post('store', 'BooksController@store')->name('store');

            // panel.book.edit
            Route::get('edit/{book}', 'BooksController@edit')->name('edit');

            // panel.book.update
            Route::put('update/{book}', 'BooksController@update')->name('update');

            // panel.book.delete
            Route::delete('delete/{book}', 'BooksController@delete')->name('delete');
        });


        Route::group([
            'prefix'    => 'author',
            'as'        => 'author.',
        ],
        function()
        {
            // panel.author.index
            Route::get('/', 'AuthorsController@index')->name('index');

            // panel.author.create
            Route::get('create', 'AuthorsController@create')->name('create');

            // panel.author.store
            Route::post('store', 'AuthorsController@store')->name('store');

            // panel.author.edit
            Route::get('edit/{author}', 'AuthorsController@edit')->name('edit');

            // panel.author.update
            Route::post('update/{author}', 'AuthorsController@update')->name('update');

            // panel.author.delete
            Route::delete('delete/{author}', 'AuthorsController@delete')->name('delete');
        });


        Route::group([
            'prefix'    => 'category',
            'as'        => 'category.',
        ],
        function()
        {
            // panel.category.index
            Route::get('/', 'CategoriesController@index')->name('index');

            // panel.category.create
            Route::get('create', 'CategoriesController@create')->name('create');

            // panel.category.store
            Route::post('store', 'CategoriesController@store')->name('store');

            // panel.category.edit
            Route::get('edit/{category}', 'CategoriesController@edit')->name('edit');

            // panel.category.update
            Route::post('update/{category}', 'CategoriesController@update')->name('update');

            // panel.category.delete
            Route::delete('delete/{category}', 'CategoriesController@delete')->name('delete');
        });

        Route::group([
            'prefix' => 'sales',
            'as'     => 'sales.'
        ],
        function () {

            //// panel.sales.index
            Route::get('/', 'SalesController@index')->name('index');

            //// panel.sales.import.form
            Route::get('import','SalesController@importForm')->name('import.form');

            //// panel.sales.upload.data
            Route::post('import','SalesController@importData')->name('upload.data');

        });



    });
